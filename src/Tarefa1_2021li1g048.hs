{- |
Module      : Tarefa1_2021li1g048
Description : Validação de um potencial mapa
Copyright   : Luis Mendes <a84675@alunos.uminho.pt>;
            : Rui Gonçalves <a101759@alunos.uminho.pt>;

Módulo para a realização da Tarefa 1 do projeto de LI1 em 2021/22.
-}
module Tarefa1_2021li1g048 where

import           LI12122

{- |
Verifica se o mapa está construido corretamente, não contendo duas peças numa
mesma posição,ter uma e só uma porta, as caixas não estarem a flutuar, existir
pelo menos um espaço vazio e conter uma base feita com blocos.
-}
validaPotencialMapa :: [(Peca, Coordenadas)] -> Bool
validaPotencialMapa [] = False
validaPotencialMapa l = verificarCrdRepetidas l
  && verificarPortas l
  && verificarCaixas l
  && verificarEspacos l
  && verificarBase l

{- |
Verifica se o mapa não contém duas peças na mesma posição
-}
verificarCrdRepetidas :: [(Peca, Coordenadas)] -> Bool
verificarCrdRepetidas [] = True
verificarCrdRepetidas
  ((_, c):t) = verificarCrdRepetidasI t c && verificarCrdRepetidas t
  where
    verificarCrdRepetidasI :: [(Peca, Coordenadas)] -> Coordenadas -> Bool
    verificarCrdRepetidasI [] _ = True
    verificarCrdRepetidasI ((_, c):t) n
      | c == n = False
      | otherwise = verificarCrdRepetidasI t n

{- |
Verfica se existem exatamente 1 porta
-}
verificarPortas :: [(Peca, Coordenadas)] -> Bool
verificarPortas [] = False
verificarPortas l = verificarPortasI l == 1
  where
    verificarPortasI :: [(Peca, Coordenadas)] -> Int
    verificarPortasI [] = 0
    verificarPortasI ((Porta, _):t) = 1 + verificarPortasI t
    verificarPortasI (_:t) = verificarPortasI t

{- |
Verifica se as caixas tem um bloco (ou outra caixa) em baixo
-}
verificarCaixas :: [(Peca, Coordenadas)] -> Bool
verificarCaixas [] = True
verificarCaixas l = verificarCaixaBloco l (moverBaixo (obterCaixasI l))
  where
    obterCaixasI :: [(Peca, Coordenadas)] -> [Coordenadas]
    obterCaixasI [] = []
    obterCaixasI ((Caixa, n):t) = n:obterCaixasI t
    obterCaixasI (_:t) = obterCaixasI t

    {- |
    Desloca todas as coordenadas para o y superior (para baixo no ecrã)
    -}
    moverBaixo :: [Coordenadas] -> [Coordenadas]
    moverBaixo [] = []
    moverBaixo ((x, y):t) = (x, y + 1):moverBaixo t

    {- |
    Verifica a existencia de uma caixa ou um bloco nas coordenadas
    -}
    verificarCaixaBloco :: [(Peca, Coordenadas)] -> [Coordenadas] -> Bool
    verificarCaixaBloco _ [] = True
    verificarCaixaBloco l (h:t) = (elem (Caixa, h) l || elem (Bloco, h) l)
      && verificarCaixaBloco l t

{- |
Encontra as dimensões do mapa
-}
obterDimensoes :: [(Peca, Coordenadas)] -> Coordenadas
obterDimensoes [] = (0, 0)
obterDimensoes ((_, (x1, y1)):t) = let (x2, y2) = obterDimensoes t
                                   in (max x1 x2, max y1 y2)

{- |
Verifica a existencia de espaços vazios
-}
verificarEspacos :: [(Peca, Coordenadas)] -> Bool
verificarEspacos [] = False
verificarEspacos l = verificarEspacosI l (obterDimensoes l)
  where
    verificarEspacosI :: [(Peca, Coordenadas)] -> Coordenadas -> Bool
    verificarEspacosI l (x, y) = verificarEspacosC l (x, y)
      || verificarEspacosL l (x, y)
      || verificarEspacosI l (max (x - 1) 0, max (y - 1) 0)

    {- |
    Verifica a existencia de espaços vazios numa coluna
    -}
    verificarEspacosC :: [(Peca, Coordenadas)] -> Coordenadas -> Bool
    verificarEspacosC [] _ = False
    verificarEspacosC l (x2, y2)
      | y2 == 0 = verificarPeca (x2, 0) l
      | otherwise =
        verificarPeca (x2, y2) l && verificarEspacosC l (x2, y2 - 1)

    {- |
    Verifica a existencia de espaços vazios numa linha
    -}
    verificarEspacosL :: [(Peca, Coordenadas)] -> Coordenadas -> Bool
    verificarEspacosL [] _ = False
    verificarEspacosL l (x2, y2)
      | x2 == 0 = verificarPeca (0, y2) l
      | otherwise =
        verificarPeca (x2, y2) l && verificarEspacosL l (x2 - 1, y2)

    {- |
    Verificar se existe alguma peça no x, y especificado
    -}
    verificarPeca :: Coordenadas -> [(Peca, Coordenadas)] -> Bool
    verificarPeca _ [] = False
    verificarPeca (x1, y1) ((p, (x2, y2)):t)
      | x1 == x2 && y1 == y2 && p /= Vazio = True
      | otherwise = verificarPeca (x1, y1) t

{- |
Verificar se a base do mapa é composta por blocos
-}
verificarBase :: [(Peca, Coordenadas)] -> Bool
verificarBase [] = False
verificarBase l = verificarBaseI l (obterDimensoes l)
  where
    verificarBaseI :: [(Peca, Coordenadas)] -> Coordenadas -> Bool
    verificarBaseI l (x, y)
      | x == 0 = (Bloco, (0, y)) `elem` l
      | otherwise = elem (Bloco, (x, y)) l && verificarBaseI l (x - 1, y)
