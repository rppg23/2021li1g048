{- |
Module      : Tarefa2_2021li1g048
Description : Construção/Desconstrução do mapa
Copyright   : Luis Mendes <a84675@alunos.uminho.pt>;
            : Rui Gonçalves <a101759@alunos.uminho.pt>;

Módulo para a realização da Tarefa 2 do projeto de LI1 em 2021/22.
-}
module Tarefa2_2021li1g048 where

import LI12122

constroiMapa :: [(Peca, Coordenadas)] -> Mapa
constroiMapa pecas = undefined

desconstroiMapa :: Mapa -> [(Peca, Coordenadas)]
desconstroiMapa mapa = undefined
