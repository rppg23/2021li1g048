{- |
Module      : Tarefa3_2021li1g048
Description : Representação textual do jogo
Copyright   : Luis Mendes <a84675@alunos.uminho.pt>;
            : Rui Gonçalves <a101759@alunos.uminho.pt>;

Módulo para a realização da Tarefa 3 do projeto de LI1 em 2021/22.
-}
module Tarefa3_2021li1g048 where

import LI12122

instance Show Jogo where
  show = undefined
