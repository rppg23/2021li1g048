{- |
Module      : Tarefa4_2021li1g048
Description : Movimentação do personagem
Copyright   : Luis Mendes <a84675@alunos.uminho.pt>;
            : Rui Gonçalves <a101759@alunos.uminho.pt>;

Módulo para a realização da Tarefa 4 do projeto de LI1 em 2021/22.
-}
module Tarefa4_2021li1g048 where

import LI12122

moveJogador :: Jogo -> Movimento -> Jogo
moveJogador jogo movimento = undefined

correrMovimentos :: Jogo -> [Movimento] -> Jogo
correrMovimentos jogo movimentos = undefined

