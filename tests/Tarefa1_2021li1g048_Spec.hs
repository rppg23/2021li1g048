module Tarefa1_2021li1g048_Spec where

import           Fixtures
import           LI12122
import           Tarefa1_2021li1g048
import           Test.HUnit

-- Tarefa 1
testsT1 = test
  [ "Tarefa 1 - Teste Valida Mapa m1r" ~: validaPotencialMapa m1 ~=? True
  , "Tarefa 1 - Teste Valida Mapa vazio" ~: validaPotencialMapa [] ~=? False
  , "Tarefa 1 - Teste Valida Mapa com 2 portas"
    ~: validaPotencialMapa [(Porta, (0, 0)), (Porta, (1, 0))]
    ~=? False
  , "Tarefa 1 - Teste interno, Invalida Mapa com dois blocos na mesma coordenada"
    ~: verificarCrdRepetidas [(Bloco, (0, 0)), (Bloco, (0, 0))]
    ~=? False
  , "Tarefa 1 - Teste interno, Valida caixas"
    ~: verificarCaixas [(Bloco, (0, 2)), (Caixa, (0, 1))]
    ~=? True
  , "Tarefa 1 - Teste interno, Valida dimensoes" ~: obterDimensoes m1
    ~=? (6, 4)
    -- Sanity Tests
  , "Tarefa 1 - Sanity test, Valida espacos" ~: verificarEspacos m1 ~=? True
  , "Tarefa 1 - Sanity test, Valida base" ~: verificarBase m1 ~=? True]
-- TODO: colocar todos os ficheiros com a mesma formatação
